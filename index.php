<?php
require_once 'vendor/autoload.php';
$loader = new Twig_Loader_Filesystem(array_merge(array_filter(glob('_patterns/*'), 'is_dir'), array_filter(glob('_patterns/*/*'), 'is_dir')));
$twig = new Twig_Environment($loader);
$template = $twig->load('site.twig');
echo $template->render(array());