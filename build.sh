PATTERNLAB_DIR="";
if [ "$PATTERNLAB_DIR" == "" ]
then
	echo "Could not build. Please enter the full path for your Pattern Lab source/_patterns directory"
	exit;
fi
JSON=composer.json
VENDOR=./vendor
EXE=composer

if [ ! -f "$JSON" ]
then
	echo "Error: No $JSON found"
	exit 1
fi

FOUND=`which $EXE`
if [ ! -x "$FOUND" ]
then
	echo "Error: executable composer not found on path"
	exit 1
fi

COMPOSER_PARMS="--optimize-autoloader"

if [ -d "$VENDOR" ]
then
	echo Invoking: $FOUND update $COMPOSER_PARMS
	$FOUND update $COMPOSER_PARMS
else
	echo Invoking: $FOUND install $COMPOSER_PARMS
	$FOUND install $COMPOSER_PARMS
fi

# No longer need this with COMPOSER_PARMS="--optimize-autoloader"
echo Dumping the psr-4 autoloader classmap.
$FOUND dumpautoload -o

if [ ! -d "_patterns" ]
then
	mkdir "_patterns";
fi

echo "Purging current patterns"
rm -rf "_patterns/*"

echo "Copying patterns over from source"
cp -R "$PATTERNLAB_DIR/" "_patterns/.";

echo "Renaming directories and filenames"
find _patterns/ -name '*.twig' -type f -exec bash -c 'new="`echo "$1" | sed -e 's/[0-9][0-9]-//g'`"; newdir=$(dirname "${new}"); mkdir -p "$newdir"; mv "$1" "$new"' -- {} \;

echo "Cleaning up empty directories"
rm -rf _patterns/*-*;
rm -rf _patterns/_patterns;

echo "Replacing 'organisms-' string in .twig files for one-to-one includes"
find . -name '*.twig' -type f -exec sed -i '' 's/organisms-//g' {} \;

echo "Replacing 'atoms-' string in .twig files for one-to-one includes"
find . -name '*.twig' -type f -exec sed -i '' 's/atoms-//' {} \;

echo "Replacing 'molecules-' string in .twig files for one-to-one includes"
find . -name '*.twig' -type f -exec sed -i '' 's/molecules-//' {} \;

echo "Replacing 'templates-' string in .twig files for one-to-one includes"
find . -name '*.twig' -type f -exec sed -i '' 's/templates-//' {} \;

echo "Adding .twig extension to all includes"
find ./ -type f -name "*.twig" -exec sed -i '' 's/{% include "\(.*\)" %}/{% include "\1.twig" %}/g' {} \;

echo "Completion!"
