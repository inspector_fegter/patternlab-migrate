# Pattern Lab Migrate - Proof-of-Concept #

### Installation ###

* Install Composer on your system
* Clone this repo to a public docroot on your local web server
* Open `build.sh` and insert the full path to your Pattern Lab `source/_patterns` folder at `PATTERNLAB_DIR="";`
* Make sure `./build.sh` is executable: `chmod +x build.sh`
* Run the build: `./build.sh` You will see a flurry of activity
* Visit the index.php to see the .twig includes working (though very ugly)